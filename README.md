# OpenCV Python - Object Tracking
Python script to track object from webcam or video file.

## Prerequisites
- Essentials:
  - Python 3.x
- Python packages:
  - `opencv-contrib-python`
  - `imutils`

## Optional Arguments
- `--video` or `-v` : Use specific video file (add path to video file). If not specified, webcam will be active.
- `--track ` or `-t` : Select OpenCV Tracker type (options: `csrt`, `kcf`, `mil`). `kcm` is the default tracker type.

## How to Run the Script
- `python object_tracking.py`, or
- `python object_tracking.py -v path/to/video-file.mp4 -t csrt`

Change *`path/to/video-file.mp4`* and *`csrt`* as your needs.
